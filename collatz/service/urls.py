from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    path('', views.formulario, name="inicio"),
    path('service/calculo/', views.Calculo.as_view(), name="calculo-view"),
    #path('service/reporte/',views.generar_reporte)
    
]

urlpatterns = format_suffix_patterns(urlpatterns)