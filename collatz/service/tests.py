from django.test import TestCase
from django.urls import reverse
from rest_framework import status
import unittest
from rest_framework.test import APITestCase


class ServiceTests(APITestCase):
    def test_secuencia_1(self):
        
        url = reverse('calculo-view')
        data = {'numero': 4}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'numeros': [4,2,1]})

    def test_secuencia_2(self):

        url = reverse('calculo-view')
        data = {'numero': 40}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'numeros': [40,20,10,5,16,8,4,2,1]})


    def test_secuencia_3(self):

        url = reverse('calculo-view')
        data = {'numero': 100}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'numeros': [ 100,50,25,76,38,19,58,29,88,44,22,11,34,17,52,26,13,40,20,10,5,16,8,4,2,1]})