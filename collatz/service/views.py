from django.shortcuts import render
from django.template import Context
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.shortcuts import redirect
from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template, render_to_string
from fpdf import FPDF, HTMLMixin
from django.utils import timezone
import os
from django.http import HttpResponse
from collatz.settings import BASE_DIR
from rest_framework import status

       

def formulario(request):
    return render(request, 'index.html')


class HtmlPdf(FPDF, HTMLMixin):
    pass


class Calculo(APIView):

    def operacion(self, numero, secuencia):

       secuencia.append(numero)

       if numero == 1:
           return secuencia

       if numero % 2 == 0:
           return self.operacion(int(numero/2), secuencia)

       return self.operacion((numero)*3 + 1, secuencia)      
    
    def post(self, request):
        if request.method == 'POST':
            if int(request.data['numero']) > 0:
                secuencia = []
                resultado = self.operacion(int(request.data['numero']), secuencia)
            else:
                return Response({"Error": "Error!...El numero debe ser mayor a 0"})

        
            hoy = timezone.now()
            params = {
                "fecha": hoy,
                "numeros": resultado
            }

            pdf = HtmlPdf()
            pdf.add_page()
            pdf.write_html(render_to_string('reporte.html', params))

            target = os.path.join(BASE_DIR, 'service', 'pdfs')
            pdf.output(target + '/reporte.pdf', 'F')

            resultado = {"numeros": resultado}

            return Response(resultado, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_403_FORBIDDEN)
